---
title: "Reprodown: An R package for reproducible data analysis"
date: 2020-09-10
categories:
    - reproducibility
tags:
    - blogdown
    - GNU make
    - hugo
---



<div id="replicability-and-reproducibility" class="section level2">
<h2>Replicability and reproducibility</h2>
<p>Reproducibility and replicability play important roles in science to confirm new
discoveries and to extend our understanding of the natural world. Both are about getting
consistent results; the former using mainly the same data, methods and code; while the
latter across studies aiming to answer the same scientific question with different
collected data and methods (National Academies of Sciences, Engineering, and Medicine;
2019).</p>
<p>The concern on reproducibility and replicability has been raised drastically in the last
decade given that several promising results in medicine and other research areas were not
able to be replicated or reproduced. In particular, Baker (2015) highlighted the
difficulty of replicating results in studies that use antibodies (“Y-shaped proteins that
bind to specified biomolecules and used to flag their presence in a sample”). The main
problem with these applications was that antibodies were not validated adequately and that
standardized information quality about them were not provided.</p>
<p>These concerns are common in different areas of research. A Nature’s survey of 1576
researchers (on Chemistry, Biology, Physics and Engineering, Earth and environment and
others) reported that that 70% of them have tried and failed to reproduce another
researcher’s experiment. These researchers thought that “more robust experiment design”,
“better statistics” and “better mentorship” could help to improve reproducibility.</p>
<p>Reproducibility can be considered a key minimum acceptable standard of research given that
replicability can not always be achieved because it depends on the size of the study,
available budget, time and other factors (Peng, 2015). Reproducibility can help to
validate data analysis, improve collaboration and detect errors or bad practices of the
analysis.</p>
</div>
<div id="reprodown" class="section level2">
<h2>Reprodown</h2>
<p><a href="https://github.com/erickchacon/reprodown">Reprodown</a> is an R package that helps to
improve reproducibility by using:</p>
<ul>
<li><a href="https://github.com/rstudio/blogdown">Blogdown</a>: An <code>R</code> package that
integrates <a href="https://rmarkdown.rstudio.com">rmarkdown</a> with <a href="https://gohugo.io">Hugo</a> to
create a website.</li>
<li><a href="https://www.gnu.org/software/make/manual/make.html">GNU make</a>: A <code>GNU</code> utility that
determines which pieces of a program need to be compiled. This is based on a file called
<code>Makefile</code> where dependencies are defined.</li>
<li><a href="https://github.com/erickchacon/scholar-docs">scholar-docs</a>: A custom <code>hugo</code> theme for a
webpage.</li>
</ul>
<p>The workflow of <code>reprodown</code> is to write the <code>.Rmd</code> files containing our data analysis
inside a sub-folder (e.g. scripts). Then the function <code>reprodown::makefile</code> will read the
<code>.Rmd</code> files to create automatically the <code>Makefile</code>. The outputs are render to <code>html</code> files
by simply running the utility <code>make</code> on the terminal.</p>
</div>
<div id="reprodown-example" class="section level2">
<h2>Reprodown example</h2>
<p>An example of a website built with <code>reprodown</code> can be found at
<a href="https://erickchacon.gitlab.io/project-web" class="uri">https://erickchacon.gitlab.io/project-web</a>. You can explore the source code at
<a href="https://gitlab.com/ErickChacon/project-web" class="uri">https://gitlab.com/ErickChacon/project-web</a>.</p>
<div class="figure">
<img src="/blog/2020-09-10-reprodown_myfiles/project-web.png" alt="" />
<p class="caption">Reprodown example</p>
</div>
</div>
<div id="reprodown-tutorial" class="section level2">
<h2>Reprodown tutorial</h2>
<div id="requirements" class="section level3">
<h3>Requirements</h3>
<p>We need to install the R packages <code>blogdown</code> and <code>reprodown</code>. We need my custom fork of
<code>blogdown</code> given that I made a pull request to add a functionality to the function
<code>blogdown:::build_rmds</code>. Hopefully, this will accepted in the future.</p>
<pre class="r"><code>remotes::install_github(&quot;ErickChacon/blogdown&quot;)
remotes::install_github(&quot;ErickChacon/reprodown&quot;)</code></pre>
<p>In addition, we also need the <code>GNU make</code> utility which comes with any GNU/Linux distribution.</p>
</div>
<div id="getting-started" class="section level3">
<h3>Getting started</h3>
<ul>
<li>Create the structure of the project (optional): The function <code>reprodown::create_proj()</code>
can be used to create the folders of our project. By default, this create the folders
<code>data</code>, <code>docs</code>, <code>scripts</code> and <code>src</code>. However, you can provide the argument <code>yaml_file</code>
with a path to a <code>yaml</code> file with a custom structure.</li>
<li>Create the website-related files: The function <code>blogdown::new_site</code> can be used to
create these files. The theme of the website is also downloaded by this function. You
can use your custom theme or other hugo theme from <a href="https://themes.gohugo.io/" class="uri">https://themes.gohugo.io/</a>. The user
does not need to work directly with most of these files, <code>blogdown</code> will take care of
this. The file <code>docs/config.toml</code> defines the metadata of your web, check this and
modify your data accordingly.</li>
</ul>
<pre class="r"><code>reprodown::create_proj()
blogdown::new_site(&#39;docs&#39;, theme = &#39;ErickChacon/scholar-docs&#39;, sample = FALSE)</code></pre>
</div>
<div id="create-your-custom-scripts" class="section level3">
<h3>Create your custom scripts</h3>
<p>Create your <code>.Rmd</code> files inside the <code>scripts</code> folder. Take into consideration the
following:</p>
<ul>
<li>The file <code>_index.Rmd</code> inside the <code>scripts</code> folder control the homepage. You can define
the title in a <code>yaml</code> header. See for example the
<a href="https://gitlab.com/ErickChacon/project-web/-/blob/master/scripts/_index.Rmd">scripts/_index.Rmd</a>
file of the web <a href="https://erickchacon.gitlab.io/project-web" class="uri">https://erickchacon.gitlab.io/project-web</a>.</li>
<li>The dependency of the files is defined in the yaml header of the <code>.Rmd</code> files. For
example, the yaml header below of the file
<a href="https://gitlab.com/ErickChacon/project-web/-/blob/master/scripts/30-process/process.Rmd">scripts/30-process/process.Rmd</a>
indicates that this files needs as input the file <code>data/cleaned/data.rds</code> and has as output
the file <code>data/processed/data.rds</code>.</li>
</ul>
<pre><code>---
title: &quot;Transform covariate&quot;
prerequisites:
    - data/cleaned/data.rds
targets:
    - data/processed/data.rds
---</code></pre>
</div>
<div id="render-the-web" class="section level3">
<h3>Render the web</h3>
<p>The web can be rendered by:</p>
<ol style="list-style-type: decimal">
<li>Creating the Makefile with the following R code: <code>reprodown::makefile()</code>. A Makefile
like <a href="https://gitlab.com/ErickChacon/project-web/-/blob/master/Makefile">this</a> will be
created.</li>
<li>Render the <code>.Rmd</code> files with the R code <code>system(</code>make<code>)</code> or running <code>make</code> on your
terminal.</li>
<li>Serve the site using <code>setwd("docs"); blogdown::serve_site(); setwd("..")</code></li>
<li>Stop serving with <code>servr::daemon_stop()</code>.</li>
</ol>
</div>
<div id="publish-and-update-automatically-your-website" class="section level3">
<h3>Publish and update automatically your website</h3>
<p>You can host your project in a remote repository to make your website available. An easy
way is to host it on <a href="https://about.gitlab.com/">gitlab</a>. Use <code>reprodown::create_gitlab_ci()</code> to create the file <code>.gitlab-ci.yml</code> that define the workflow to create the website.</p>
<pre class="r"><code>reprodown::create_gitlab_ci()</code></pre>
<p>In addition, I suggest to avoid pushing the <code>data</code> folder content to avoid publishing
confidential data or having issues with big files. The same should be done with the <code>docs/public</code> folder given that it will be
automatically created by the gitlab workflow. This can be done by by creating a <code>.gitignore</code> file with content:</p>
<pre><code># Public content
/docs/public

# Data sub-folders content
/data/raw/*
!/data/raw/.gitkeep
/data/modelled/*
!/data/modelled/.gitkeep
/data/processed/*
!/data/processed/.gitkeep
/data/cleaned/*
!/data/cleaned/.gitkeep</code></pre>
<p>Push all your folder content to a gitlab repository and you will have available a website
that will be automatically updated each time you push a commit.</p>
</div>
</div>
<div id="references" class="section level2">
<h2>References:</h2>
<ol style="list-style-type: decimal">
<li>Baker, M. (2016). A Nature Survey Lifts the Lid on How Researchers View the ‘Crisis’ Rocking Science and What They Think Will Help. Nature, 3.</li>
<li>Baker, M. (2015). Blame it on the antibodies. Nature, 521(7552), 274.</li>
<li>National Academies of Sciences, Engineering, and Medicine. (2019). Reproducibility and replicability in science. National Academies Press.</li>
<li>Peng, R. (2015). The reproducibility crisis in science: A statistical counterattack. Significance, 12(3), 30-32.</li>
</ol>
</div>
