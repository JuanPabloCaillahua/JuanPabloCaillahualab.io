---
title: Home
name: Juan Pablo Caillahua Lazo
date: 2017-02-20T15:26:23-06:00

categories: [one]
tags: [two,three,four]
interests: [
    "Spatio-Temporal Modelling",
    "Bayesian Statistics",
    "Computational Intensive Methods",
    "Causal Modelling",
    "Data Visualization",
    "Reproducibility"
  ]
applications: [
    "Epidemiology",
    "Climate Change",
    "Extreme Events",
    "Others Complex Systems"
  ]
education:
  first:
    course: PhD in Statistics and Epidemiology
    university: Lancaster University
    years: 2016 - 2019
  second:
    course: MSc in Statistics
    university: Lancaster University
    years: 2015 - 2016
  third:
    course: BSc in Engineering Statistics
    university: Universidad Nacional de Ingeniería
    years: 2008 - 2013
---


I am researcher in *Spatial Statistics*. My research focusses on developing
spatio-temporal statistical methods to model environmental and health data. I use open
source software to implement my work such as Julia, R and Python. Check my
[GitHub](https://github.com/ErickChacon) and [GitLab](https://gitlab.com/ErickChacon)
accounts to know more about my current work.
